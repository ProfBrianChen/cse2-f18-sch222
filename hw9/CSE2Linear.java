///////////////
/// CSE02 Homework 09
/// Sarah Horne 
/// November 27, 2018
///////////////
 
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random; 


public class CSE2Linear{//Start Class
    
    public static void main(String[] args){//Start main
      
      //Declare instance of the scanner class
      Scanner myScanner = new Scanner (System.in);
      
      //First array being declared- will be used to hold the 15 grades
      int [] arrayOne = new int[15];
      
      //Tells the user exactly what it wants
      System.out.println("You will be prompted to input 15 numbers that represent your 15 grades. Only integers from 0-100 will be accepted. Please"
                        + " enter in order from least to greatest.");
      
      
      //For statment that goes through the 15 array locations asking for an integer in each spot
      for (int i = 0; i < 15; i++) {
        int j = i +1; //Used in the system out statement to show grade 1: instead of grade 0: and so on 
        System.out.print ("Grade " + j + ": " );
        boolean numberBoolean = myScanner.hasNextInt();
        
        //Nested if-else statements
        //Outer if-else tests if the number is integer for not
        if(numberBoolean){//Is integer
           int numberInt = myScanner.nextInt(); //turns boolean to actual number value for int
          
          //Middle if-else statement that tests if the number is between 0 and 100
          if (numberInt >= 0 && numberInt <= 100){//Is inbetween 0 and 100
            
            arrayOne[i] = numberInt; //Assigns the integer to the i spot in the array
            
            //if-else statment for checking if the next element is greater than the last
            if (i > 0){ //i has to be greater than 0 because you can't compare elements 0 and -1 
              if (arrayOne[i-1] > arrayOne[i]){ //If spot i is less than i-1 (which is wrong) 
                System.out.println ("Error, please enter the next greatest number");
                i--;
            }
            }else 
            {
              arrayOne[i] = numberInt;
            }
            
            
            
          }else{//Value is less than 0 or greater than 100
            System.out.println ("Error, please enter an int from 0-100!");
            i--;
          }
          
       }else{//Is not integer
         System.out.println ("Error, please enter an int!");
         myScanner.next();
         i--;
      }
        
        
      }
      
      System.out.println("The final array is: " + Arrays.toString(arrayOne)); //Prints out final array
      
      
      System.out.print("Enter a grade to be searched for: "); //Enter a grade 
      int searchGrade = myScanner.nextInt();
      
      binarySearch(arrayOne, searchGrade);
      
      
      
      scramble(arrayOne);
      
      System.out.print("Enter a grade to be searched for: "); //Enter a grade 
      int linearSearch = myScanner.nextInt();
      
      linearSearch(arrayOne, linearSearch);
      
      
      
      }//End Main
      
      //Binary Search- code used from Professor Carr's code
      
      	public static void binarySearch(int[] list, int key) {
          
          int low = 0;
          int high = list.length-1;
          int counter = 0; 
          
          while(high >= low) {
            counter++;
            int mid = (low + high)/2;
            
            if (key < list[mid]) {
              high = mid - 1;
            }
            else if (key == list[mid]) {
              System.out.println(key + " was found in " + counter + " iterations");
              break;
            }
            else {
              low = mid + 1;
            }
            
            if (key > list[mid]){
              System.out.println(key + " was not found in " + counter + " iterations");
            }
          }
        }
      
   //Scramble Array
   public static void scramble(int[] list){//Start scramble
     
      //gets random number
     Random rand = new Random();
     int n = rand.nextInt(15);
      
      //Randomizes the order of the array
      for (int i = 0; i < 15; i++){ 
        int index = rand.nextInt(i + 1);
        int a = list[index];
        list[index] = list[i];
        list[i] = a;
      } 
    
      System.out.println("Scrambled List: " + Arrays.toString(list));  
      
      
    }//End scramble
    
  
  //Taken from Professor Carr's Code
  public static void linearSearch(int[] list, int key) {//Start linearSearch 
    int count = 0;
    for (int i = 0; i < list.length; i++) 
    {
      count ++;
      if (key == list[i]){
        System.out.println("The number " + key + " was found after " + count + " iterations.");
        count --;
        break; 
      }
      
    }
    
    if (count == 15){
      System.out.println("The number " + key + " was not found after " + count + " iterations.");
    }
     
    
      
    
  }//End linearSearch
  
  
    
    
}//ends class