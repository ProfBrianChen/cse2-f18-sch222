///////////////
/// CSE02 Homework 09
/// Sarah Horne 
/// November 27, 2018
///////////////


import java.util.Scanner;
import java.util.ArrayList; 
import java.util.Random; 


public class RemoveElements{
  
  public static void main(String [] arg){
    
    Scanner scan = new Scanner(System.in);
    
    int num [] = new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
	
    do{
      System.out.println("Random input 10 ints [0-9] ");
      num = randomInput();
      
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      
      if (index >= 0 && index <= 9){
        System.out.println("Index " + index + " element is removed");
        
      }else {
        System.out.println("The element index is not valid");
      }
      newArray1 = delete(num,index);
      
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
  	 
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
	
    }while(answer.equals("Y") || answer.equals("y"));
  
  }//End main
 
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput(){
    //Generates random numbers 0-9 in an array of 10
    
    int[] numbers = new int[10];
    for(int i = 0; i < 10; i++) {
      numbers[i] = (int)(Math.random()*9 + 1);
    }
    return numbers; 
  }
  
    public static int[] delete(int[] list, int pos){//Start delete
     
      if (pos >= 0 && pos <= 9){//Checks if pos is in range
        int list2 [] = new int[list.length - 1];
        
        for(int i = 0; i < pos; i++){ //goes through all the elements in the array
          list2[i] = list [i];
        }
        for (int j = pos+1; j < list.length; j++){
          list2[pos] = list [j];
          pos++;
        }return list2; 
        
      } else {//if not in range- returns original list
        return list; 
      }
      
     
  }//End delete
  
  
  public static int [] remove (int [] list, int target){
  
    
    int counter = 0;
    for (int i = 0; i < list.length; i++){
      if (list[i] != target){
        counter ++;
      }
    }
    
    int [] list2 = new int [counter];
    int counter2 = 0;
    
    for (int j = 0; j< list.length; j++){
      if (list[j] != target){
        list2[counter2] = list [j];
        counter2++;
      }
    }
    return list2;

   
  }
    
  
}//End Class
