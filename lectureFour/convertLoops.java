import java.util.Scanner;

public class convertLoops{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    // Convert a simple while loop to a for loop
    int i,j;
    for (i = 0; i < 2; i++){
      for (j = 0; j < 3; j++){
        System.out.println("i: " + i + " j: " + j);
      }
    }
    myScanner.nextLine();
    
    i = 0; 
    while(i < 2){
      j = 0;
      while (j < 3){
        System.out.println("i: " + i + " j: " + j);
        j++;
      }
      i++;
    }
 
    /*myScanner.nextLine();
    
    // Convert a this same for loop to a do-while loop
    i = 0; 
    do{
      j = 0;
      do{
        System.out.println("i: " + i + " j: " + j);
        j++;
      }while(j < 3);
      i++;
    }while(i < 2);
    */
  }
  
}