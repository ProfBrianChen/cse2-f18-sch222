import java.util.Arrays;

public class copyArrays {

	public static void main(String[] args) {
		int[] myList1 = new int[5];
		int[] myList2;
		
		for(int i = 0; i < myList1.length; i++) {
			myList1[i] = i;
		}
		System.out.println(Arrays.toString(myList1));
		
		myList2 = myList1;
		System.out.println(Arrays.toString(myList2));
		System.out.println(myList1);
		System.out.println(myList2);
		myList2[4] = -100;
		System.out.println(Arrays.toString(myList2));
		
		System.out.println(Arrays.toString(myList1));
		
		int[] myList3 = new int[5];
		//int i = 0;
		for(int i = 0; i < myList1.length;i++){
			myList3[i] = myList1[i];
		}
		System.out.println(myList3);
		System.out.println(myList1);
	}
}
