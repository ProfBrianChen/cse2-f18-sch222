import java.util.Arrays;
public class arraysIntro {

	public static void main(String[] args) {
		// declare an array variable, 
		//create an array of ten elements of double type, and
		// assign its reference to that variable
		double[] myList = new double[10];
		// Modify the array to take a different type 
		// Assign values: arrayRefVar[index] = value
		myList[0] = 5.6; 
		myList[1] = 4.5; 
		myList[2] = 3.3; 
		myList[3] = 13.2; 
		myList[4] = 4.0;
		myList[5] = 34.33;
		myList[6] = 34.0; 
		myList[7] = 45.45; 
		myList[8] = 99.993; 
		myList[9] = 11123; // myList.length-1 (n - 1)
		
		//To access array variables, we follow the same pattern
		// as assigning them
		//double printArrayElem = myList[5];
		//System.out.println(myList[5]);
		
		
		for (int i = 0; i < myList.length; i++) { 
			myList[i] = i;
		}
		//System.out.println(myList); // WRONG
		System.out.println(Arrays.toString(myList));
		
    
    
		// Array initializers
		double[] myList2 = {1.9, 2.9, 3.4, 3.5};
		System.out.println(Arrays.toString(myList2));
		System.out.println(myList2.length);
		// NOT: System.out.println(myList2);
		// This is equivalent to
		//double[] myList3 = new double[4]; 
		//myList3[0] = 1.9;
		//myList3[1] = 2.9;
		//myList3[2] = 3.4; 
		//myList3[3] = 3.5;
	}
}