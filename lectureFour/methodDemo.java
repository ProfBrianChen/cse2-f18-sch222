import java.util.Scanner;

public class methodDemo{
	public static void main(String[] arg){
		final int MYINT1 = 2;
		final int MYINT2 = 3;
		final double MYDOUBLE1 = -3.14;
		final double MYDOUBLE2 = 1.5708; // ~pi/2
		final double MYDOUBLE3 = 2.9;
		final String MYSTRING = "the output is ";
		// What happens when we try to change a constant?
		//MYINT1 = 4;
		
		int power = (int) Math.pow(MYINT1,MYINT2); // We have to explicitly cast
		                                           // to an integer here! Why?
		System.out.println("For 2^3, " + MYSTRING + power + ".");
		
		int ceil = (int) Math.ceil(MYDOUBLE3); // We explicitly cast again!
		System.out.println("For ceil" + MYDOUBLE3 + ", " + MYSTRING + ceil + ".");
		
		int nextUp = (int) Math.nextUp(MYDOUBLE2); // Why don't we cast here?
		System.out.println("When rounding " + MYDOUBLE2 + MYSTRING + nextUp + ".");
		
		double abs = Math.abs(MYDOUBLE1);
		System.out.println("When calculating abs("  + MYDOUBLE1 + "), " + MYSTRING + abs + ".");
		
		double sin = Math.sin(MYDOUBLE2);
		System.out.println("When calculating sin("  + MYDOUBLE2 + "), " + MYSTRING + sin + ".");
		
	
  /* Chars are integers! */
		//char myChar = 'a';
		char myChar = 99;
		System.out.println("This is the char a: " + myChar);
		myChar = myChar++;
		System.out.println("This is the char b: " + myChar++);
    
    
    boolean myBool = 5 < 2 + (double) 7 && 4 + 6 >= 2;
		System.out.println("The value of myBool is " + myBool);
    
    int myCounter = 1;
		//myCounter = myCounter + 1;
		//System.out.println("The value of myCounter is now: " + myCounter);
		System.out.println("Using preincrement, the value of myCounter becomes: " + ++myCounter);
		System.out.println("Using postincrement, the value of myCounter becomes: " + myCounter++);
    
    double val1; int val2;
		val1 = 2.3;
		val2 = 9;
		int val3 = (int) val1 * val2 - (int) val1;
		System.out.println("val 3 = " + val3);
    int val4 = (int)(val1 * val2 - val1);
    System.out.println("val 4= " + val4);
    
    
    double myValue1 = 2.0;
		double myValue2 = 1.3;
		boolean myValue1IsBigger = myValue1 > myValue2;
		System.out.println("The truth value of myValue1IsBigger: " + myValue1IsBigger);
    
    boolean isVal1StillBigger = myValue1 > myValue2+1;
		// This final expression gets evaluated as follows:
		// bool val1IsMuchBigger= myVal1 > (myVal2+1);
		System.out.println("The truth value of isVal1StillBigger: " + isVal1StillBigger);
    
    boolean myBool1 = true, myBool2 = false, myBool3 = true, myBool4 = false;
    boolean complexCompare2 = ((myBool1 == myBool2) != myBool3) == myBool4;
		// (((myBool1 == myBool2) != myBool3) == myBool4);
		System.out.println("The truth value of our complex comparison is: " + complexCompare2);
    
		//Logical operators
		int age = 20;
		boolean hasCreditCard = true;
		boolean canRentCar = age >= 25 || hasCreditCard == true;
    //This gets evaluated in the following way:
		//  boolean identityVerified = ((age == 25) \&\& (hasCreditCard == true));
		System.out.println("This person can rent a car: " + canRentCar);
    
    // Equality of strings
		String string1 = "Hello";
		String string2 = "Hello";
		String string3 = "He";
		String string4 = "llo";
		
		System.out.println("The first and second string are equal: " + (string1 == string2));
		
    String string5 = string3 + string4;
		System.out.println("This is string5: " + string5);
		System.out.println("This is string1: " + string1);
		
		System.out.println("The first and fifth string are equal: " + (string1 == string5));
		
		System.out.println("The first and fifth string are equal: " + string1.equals(string5));


  
  }
	
	
	
}