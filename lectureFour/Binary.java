
public class Binary {
	public static int binarySearch2(int[] list, int key) {
		// Note that this implementation DOES loop through
		// and continue to search for an element.  This is
		// our second, and correct, implementation of binary 
		// search
		int low = 0;
		int high = list.length-1;
		while(high >= low) {
			int mid = (low + high)/2;
			if (key < list[mid]) {
				high = mid - 1;
			}
			else if (key == list[mid]) {
				return mid;
			}
			else {
				low = mid + 1;
			}
		}
		return -1;
		//return -low - 1;
		// When a key is not found, low is the insertion point where
		// a key would be inserted to maintain the order of the list.
	}


	public static int binarySearch1(int[] list, int key) {
		// Note that this implementation does NOT loop
		// through and continue to search for an element.
		// This is our first iteration of implementing the 
		// binary search.
		int low = 0;
		int high = list.length-1;
		int mid = (low + high)/2;
		if (key < list[mid]) {
			high = mid - 1;
		}
		else if (key == list[mid]) {
			return mid;
		}
		else {
			low = mid + 1;
		}
		return -1;
		// What is the problem with this first attempt at 
		// implementing the binary search?
	}

	public static void main(String[] args) {
		int[] list = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
		//int key = 5;
		int key = 6;
    
    int low = 0;
		int high = list.length-1;
    int iterations = 0; 
    
		while(high >= low) {
			int mid = (low + high)/2;
			if (key < list[mid]) {
				high = mid - 1;
        iterations ++; 
			}
			else if (key == list[mid]) {
        iterations ++; 
				System.out.println(mid);
        continue; 
			}
			else {
				low = mid + 1;
        iterations ++; 
			}
		}
    System.out.println("Iterations: " + iterations);
		//return -1;
		//int indKey = binarySearch2(myList, key);
		//System.out.println(indKey);
	}

}