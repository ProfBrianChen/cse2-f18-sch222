import java.util.Scanner;

public class LoopsDemo {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int i = 0, j;
		while (i < 5) {
			j = 0;
			while (j < 6) {
				System.out.println("Matrix entry (" + i + "," + j+ ")");
				j++;// What if we remove our increments?
			}

			i++;
		} 
		
		// What if we reuse i?
		i = 0; 
		while (i < 5) {
			i++;
			//continue;
			while (i >= 0) {
				System.out.println("i: " + i);
				i--;
				
			}
			break;
		}
		
    
		// How do break/continue statements affect nested loops?
			// Put a break in the innermost loop above?  Outermost?
			// What about a continue?
		
		// Nested Loops
		
		for (int k = 1; k <=5; k++) {
			for (int m = 1; m <= 10; m++) {
				System.out.print("*");
			}
			System.out.println();
			
		}
		
    myScanner.nextLine();
		
		//What is the difference between this nested loop and the previous one?
		for (int kk = 1; kk <=5; kk++) {
			for (int mm = 1; mm <= kk; mm++) {
				System.out.print("*");
			}
			System.out.println();
			
		}
	}

}