import java.util.Arrays;
import java.util.Scanner;

// Given a set of numbers, find the average, and determine all 
// numbers above the average
public class AnalyzeNumbers {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the number of items: ");
		
		int n = input.nextInt();
		double [] numbers = new double[n];
		//double myDoublePrimitive = input.nextDouble();
		
    System.out.println("Enter the items");
    
    for(int i = 0; i < numbers.length; i++) {
			numbers[i] = input.nextDouble();
		}
    
		System.out.println(Arrays.toString(numbers));
		double sum = 0;
		for(int i = 0; i < numbers.length; i++) {
			sum = sum + numbers[i];
		}
		System.out.println("The sum is: " + sum);
    
    double average = 0;
    
    average = sum/numbers.length;
    
    System.out.println("The average is:" + average);
		
		// Complete the rest of the code to determine the average 
		// and then to determine all numbers greater than the average.
	}
}