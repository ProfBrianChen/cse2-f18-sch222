//////////////////////////
/// Sarah Horne 
/// September 7, 2018
/// CSE 02
/// Lab 2- Cyclometer
///
/// Purpose of lab: a bicycle cyclometer (meant to measure speed, distance, etc.) 
/// records two kinds of data, the time elapsed in seconds, and the 
/// number of rotations of the front wheel during that time
//////////////////////////

public class Cyclometer{//Start Class
  
  public static void main (String args[]){//Start Main
  
    int secsTrip1=480; //number of seconds in trip 1
    int secsTrip2=3220; //number of seconds in trip 2
    int countsTrip1=1561; //number of rotations in trip 1
    int countsTrip2=9037; //number of rotations in trip 2
    
    
    //Line 21 assigns a given about the bike that the computer did not already know
    double wheelDiameter=27.0, //The diameter of the wheel on the bike
    PI=3.14159, //The mathematical symbol PI but assigned a few digits of it's long number
    //Lines 25 through 27 assign mathematical conversions 
    feetPerMile=5280, //Number of feet in every mile
    inchesPerFoot=12, //Number of inches in every foot
    secondsPerMinute=60; //Number of seconds in each minute
    double distanceTrip1; //The distance of trip 1 measured with decimal points because it is a double
    double distanceTrip2; //The distance of trip 2 measured with decimal points because it is a double
    double totalDistance; //The total distance between trip 1 and trip 2
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
   
   	//Ran program up to this point- here are the values collected
    //Trip 1 took 8.0 minutes and had 1561 counts.
    //Trip 2 took 53.666666666666664 minutes and had 9037 counts.
  	
    distanceTrip1=countsTrip1*wheelDiameter*PI; //distance in inches, for each count a rotation of the wheel travels the diameter in inches times PI
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Same as two lines of code above, but instead it is put into one statement 
	  totalDistance=distanceTrip1+distanceTrip2; //Distance bewtween trips 1 and 2 added together
    
    
	  //Print out the output data.
    System.out.println("Trip 1 was "+ distanceTrip1 +" miles"); //Prints out distance 1 in miles
	  System.out.println("Trip 2 was "+ distanceTrip2 +" miles"); //Prints out distance 2 in miles
	  System.out.println("The total distance was "+ totalDistance +" miles"); //Prints out total distance in miles
    
    
  }//End Main
  
}//End Class 


