//////////////////////////
/// Sarah Horne 
/// October 10, 2018
/// CSE 02
/// Homework 5- Poker
///
//////////////////////////


import java.lang.Math;
import java.util.Scanner;

public class Poker{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 

    Scanner myScanner = new Scanner( System.in );

    int fourOfAKind = 0; //Will add 1 every time there are 4 of a kind
    int threeOfAKind = 0; //Will add 1 every time there are 3 of a kind
    int onePair = 0; //Will add 1 every time there is 1 pair
    int twoPairs = 0; //Will add 1 every time there are 2 pairs
    int counter = 1; //counter for while class, gets incremented each hand
    int i = 0;
    
    while (i < 1){
      System.out.println ("How many hands would you like to draw?");
      boolean nmbrOfHands = myScanner.hasNextInt();
      if (nmbrOfHands){
        
        i++;
      } else 
      {
        System.out.println ("Error, please enter an int!");
        myScanner.next();
      }
    }

    int nmbrOfHands = myScanner.nextInt();
    

while (counter <=nmbrOfHands)
    {
    
    //find 5 random cards in a deck of 52
    int card1 = (int)(Math.random()*(52+1))+1;
    int card2 = (int)(Math.random()*(52+1))+1;
    int card3 = (int)(Math.random()*(52+1))+1;
    int card4 = (int)(Math.random()*(52+1))+1;
    int card5 = (int)(Math.random()*(52+1))+1;


    //while statements to make sure no cards are the same face and value, if they are a new number is selected for one of them
    while (card1 == card2){
    card2 = (int)(Math.random()*(52+1))+1;
    }

    while (card1 == card3){
    card3 = (int)(Math.random()*(52+1))+1;
    }

    while (card1 == card4){;
    card4 = (int)(Math.random()*(52+1))+1;
    }

    while (card1 == card5){
    card5 = (int)(Math.random()*(52+1))+1;
    }

    while (card2 == card3){
    card3 = (int)(Math.random()*(52+1))+1;
    }

    while (card2 == card4){
    card4 = (int)(Math.random()*(52+1))+1;
    }

    while (card2 == card5){
    card5 = (int)(Math.random()*(52+1))+1;
    }

    while (card3 == card4){
    card4 = (int)(Math.random()*(52+1))+1;
    }

    while (card3 == card5){
    card5 = (int)(Math.random()*(52+1))+1;
    }

    while (card4 == card5){
    card5 = (int)(Math.random()*(52+1))+1;
    }
  
    //Gets numbers used for if/else statements
    card1= card1 % 13;
    card2 = card2 % 13;
    card3 = card3 % 13;
    card4 = card4 % 13;
    card5 = card5 % 13;

  
    if (card1 == card2) //One pair (card 1 and card 2)
    { 
       if (card2 == card3) //Three of a kind (card 1, card 2, and card 3)
       {
         if (card3 == card4)//Four of a kind (card 1, card 2, card 3, and card 4)
         { 
            fourOfAKind += 1;
            counter++;
         }
         else if (card3 == card5)//Four of a kind (card 1, card 2, card 3, and card 5)
         { 
            fourOfAKind +=1;
            counter++;
         }
         else if (card3 != card4 && card3 != card5 ) //Three of a kind (card 1, card 2, and card 3)
         { 
             threeOfAKind += 1;
             counter++;
         }
         else if ((card3 != card4 && card3 != card5)  && card4 == card5)//Three of a kind and a pair (card 1, card 2, and card 3) (card 4 and card 5)
         {
              onePair +=1;
              threeOfAKind +=1;
              counter++;
         }
      }
      else if (card3 == card4)//one pair (card 3 and card 4)
      { 
         if (card1 != card5  && card4 != card5) //Two pairs (card 3 and card 4) and (card 1 and card 2)
         {
              twoPairs += 1;  
              counter++;
         }
         else if (card1 != card5 && card4 == card5) //Three of a kind (card 3, card 4, card 5) and a pair (card 1 and card 2)
         { 
               onePair +=1;
               threeOfAKind +=1;
               counter++;
         }
      }  
      else if (card4 == card5)//Two pairs (card 4 and card 5) and (card 1 and card 2)
      {
          twoPairs +=1;
          counter++;
      }
          onePair +=1;//Pair from original if statement
          counter++;
    }
    else if (card2 == card3) //One pair (card 2 and card 3)
    { 
       if (card3 == card4)//three of a kind (card 2, card 3, and card 4)
       { 
           if (card4 == card5)//Four of a kind (card 2, card 3, card 4, and card 5)
           { 
             fourOfAKind += 1;
             counter++;
           }
           else if (card1 != card5)//Three of a kind (card 2, card 3, card 4)
           { 
              threeOfAKind += 1;
              counter++;
           }
           else if (card1 == card5) //One pair (card 1 and card 5) and three of a kind (card 2, card 3, and card 4)
           { 
               threeOfAKind +=1;
               onePair +=1;
               counter++;
           }
        }
        else if (card4 == card5 && card1!= card4) //Two pairs (card 2 and card 3) (card 4 and card 5)
        { 
             twoPairs += 1;
             counter++;
        }
        else if (card1 == card4 && card1 != card5)//Two pairs (card 2 and card 3) (card 1 and card 4) 
        {  
              twoPairs += 1;
              counter++;
        }
        else if (card1 == card4 && card1 == card5) //Three of a kind (card 1, card 4, and card 5) and a pair (card 2 and card 3)
        {  
              threeOfAKind +=1;
              onePair +=1;
              counter++;
        }
        else //One pair (card 2 and card 3)
        {
              onePair += 1;
              counter++;
        }
     }
     else if (card3 == card4) //One pair (card 3 and card 4)
     {
          if (card4 == card5) //Three of a kind (card 3, card 4, and card 5)
          {
               threeOfAKind += 1;
               counter++;
          }
          else if (card1 == card5) //Two pairs (card 1 and card 5) (card 3 and card 4)
          {
               twoPairs +=1;
               counter++;
          }
          else if (card2 == card5) //Two pairs (card 2 and card 5) (card 3 and card 4)
          {
                twoPairs +=1;
                counter++;
          }
          else//One pair (card 2 and card 3) 
          {
                 onePair += 1;
                 counter++;
          }     
      }
      else if (card4 == card5) //One pair (card 4 and card 5)
      {
         onePair +=1;
         counter++;
      }
      else if (card1 == card3) //One pair (card 1 and card 3)
      {
          if (card3 ==card4 && card3 == card5) // Four of a kind (card 1, card 3, card 4, and card 5)
          {
              fourOfAKind +=1;
              counter++;
          }
          else if (card3 == card4 && card3 != card5)//Three of a kind (card 1, card 3, and card 4)
          {
              threeOfAKind +=1;
              counter++;
          }
          else if (card3 == card4 && card3 != card5 && card2 == card5) //Three of a kind (card 1, card 3, and card 4) and a pair (card 2 and card 5)
          {
              threeOfAKind +=1;
              onePair +=1;
              counter++;
          } 
          else if (card3 != card4 && card2 != card4 && card4 == card5) //Two pairs (card 1 and card 3) (card 4 and card 5)
          {
              twoPairs +=1;
              counter++;
          }
          else if (card2 == card4 && card2 != card5) //Two pairs (card 1 and card 3) (card 4 and card 2)
          { 
              twoPairs +=1;
              counter++;
          }
          else if (card2 ==card4 && card2 == card5) //Three of a kind (card 2, card 4, and card 5) and a pair (card 1 and card 3)
          { 
              threeOfAKind +=1;
              onePair +=1;
              counter++;
          }
          
      }
      else if (card2 == card4) //One pair (card 2 and card 4)
      {
          if (card2 == card5) //Three of a kind (card 2, card 4, and card 5)
          {
              threeOfAKind +=1;
              counter++;
          }
          else if (card1 != card5 && card3 != card5) //One pair (card 2 and card 4)
          {
              onePair +=1;
              counter++;
          }
          else if (card1 != card2 && card1 != card3 && card1 == card5) //Two pairs (card 2 and card 4) and (card 1 and card 5)
          {
              twoPairs +=1;
              counter++;
          }
          else if (card3 != card2 && card3 != card1 && card3 == card5) //Two pairs (card 2 and card 4) and (card 3 and card 5)
          {
              twoPairs +=1;
              counter++;
          }
      }
      else if (card3 == card5)//One pair (card 3 and card 5) 
      {
          onePair +=1;
          counter++;
      }
      else if (card1 == card4) //One pair (card 1 and card 4)
      {
          if (card4 != card5 && card4 != card3) //One paur (card 1 and card 4)
          {
              onePair +=1;
              counter++;
          }
          else if (card2 == card5 && card2 != card1 && card2 != card3) //Two pairs (card 1 and card 4) and (card 2 and card 5)
          {
              twoPairs +=1;
              counter++;
          }
          else if (card3 == card5 && card3 != card1 && card3 != card2) //Two pairs (card 1 and card 4) and (card 3 and card 5)
          {
              twoPairs+=1;
              counter++;
          }
      }else if (card2 == card5) //One pair (card 2 and card 5)
      {
          onePair +=1;
          counter++;
      }else if (card1 == card5) //One pair (card 1 and card 5)
      {
          onePair +=1;
          counter++;
      }else //All other cases
      {
       counter++;
      }
}
    
    System.out.println ("Number of loops = " + nmbrOfHands); //outputs number of loops

    //finds probability for each of the different things being looked for 
    double probFourOfAKind = (double) fourOfAKind / (double) nmbrOfHands;
    
    double probThreeOfAKind = (double) threeOfAKind / (double) nmbrOfHands;

    double probOnePair = (double) onePair/ (double) nmbrOfHands;
    
    double probTwoPairs = (double) twoPairs/ (double) nmbrOfHands;

    
    //Outputs probabilities 
    System.out.println ("Probability of four of a kind = " + probFourOfAKind);

    System.out.println ("Probability of three of a kind = " + probThreeOfAKind);

    System.out.println ("Probability of one pair= " + probOnePair);
    
    System.out.println ("Probability of two pairs= " + probTwoPairs);





}//end main

}//end class
