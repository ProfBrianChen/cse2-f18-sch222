///////////////
/// CSE02 Methods Lab07
/// Sarah Horne 
/// November 2, 2018
///////////////
 
import java.util.Random;
import java.util.Scanner;


  public class Methods{//Start Class
    
    public static void main(String[] args){//Start main
      
      Scanner myScanner = new Scanner(System.in); //Create object instance
      
      int intTest = 1;
      int testDo = 1;
      int input = 0;
      

      do{//Start Do

        //Phase 1

        System.out.println("Phase One Sentence:");
        System.out.println("The " + adjective() + " " + subjectNoun() + " " + verb() + " the "+ adjective() + " " + objectNoun() + ".");
 
        //Phase 2

        System.out.println("Phase Two Sentences:");
        finalSentence();

     
      while(intTest < 2){//Start While

      System.out.println("Would you like to generate another sentence? press 1 if yes, and 2 if no");
      boolean correctInt = myScanner.hasNextInt(); //checks input

        if (correctInt){//Start if
          input = myScanner.nextInt(); 
          
          if (input == 1){ //input in range
            intTest++; //increments
          }

          
          else if (input == 2){//Input not in range
            intTest++;
            testDo++;
          }

        }//end if

        else if (input < 1 || input> 2){//Start else if
          myScanner.next();
          System.out.println("Error: Invalid input" );
        }//End else if 

        
        else {//Start else
            myScanner.next();
            System.out.println("Error: Invalid input");

        }//End else

     }//End while

         intTest--;
        
      }//End do

      while (testDo < 2); 
    
    }//ends main


    public static String subjectNoun(){//Start noun

       Random randomGenerator = new Random();
       int subjectNounInt = randomGenerator.nextInt(10);


      String subjectNounString = "";
       switch (subjectNounInt) {//Start Switch
        case 0: subjectNounString = "mountain";
          break;
        case 1: subjectNounString = "cookie";
          break;
        case 2: subjectNounString = "socks";
          break;
         case 3: subjectNounString = "backpack";
           break;
         case 4: subjectNounString = "car";
           break;
         case 5: subjectNounString = "girl";
          break;
        case 6: subjectNounString = "boy";
          break;
         case 7: subjectNounString = "friend";
           break;
         case 8: subjectNounString = "student";
           break;
         case 9: subjectNounString = "computer";
           break;         

       }//ends switch
       return subjectNounString;

    }//ends noun

   

     public static String adjective(){//Start adjective

       Random randomGenerator = new Random();
       int adjectiveInt = randomGenerator.nextInt(10);

      String adjectiveString = "";

       switch (adjectiveInt) {//Start switch
         case 0: adjectiveString = "furry";
          break;
         case 1: adjectiveString = "shiny";
          break;
        case 2: adjectiveString = "huge";
          break;
         case 3: adjectiveString = "friendly";
           break;
         case 4: adjectiveString = "soft";
           break;
         case 5: adjectiveString = "smelly";
          break;
        case 6: adjectiveString = "obnoxious";
          break;
         case 7: adjectiveString = "happy";
           break;
         case 8: adjectiveString = "slow";
           break;
         case 9: adjectiveString = "bright";
           break;          

       }//Ends switch

       return adjectiveString;

    }//Ends adjective

    
   
     public static String verb(){//Start Verb

       Random randomGenerator = new Random();
       int verbInt = randomGenerator.nextInt(10);

       
      String verbString = "";

       switch (verbInt) {//Start switch

        case 0: verbString = "ran";
          break;
        case 1: verbString = "jumped";
          break;
        case 2: verbString = "skipped";
          break;
         case 3: verbString = "sang";
           break;
         case 4: verbString = "moved";
           break;
         case 5: verbString = "ate";
          break;
        case 6: verbString = "fixed";
          break;
         case 7: verbString = "opened";
           break;
         case 8: verbString = "went";
           break;
         case 9: verbString = "drank";
           break;          
       }//End switch

       return verbString;

    }//End Verb

    

     public static String objectNoun(){//PbjectNoun

       Random randomGenerator = new Random();
       int objectNounInt = randomGenerator.nextInt(10);

       
      String objectNounString = "undefined";

       switch (objectNounInt) {//Start Switch
       case 0: objectNounString = "lion";
          break;
        case 1: objectNounString = "person";
          break;
        case 2: objectNounString = "tree";
          break;
         case 3: objectNounString = "flower";
           break;
         case 4: objectNounString = "house";
           break;
         case 5: objectNounString = "ball";
          break;
        case 6: objectNounString = "car";
          break;
         case 7: objectNounString = "swing";
           break;
         case 8: objectNounString = "house";
           break;
         case 9: objectNounString = "squirrel";
           break;          
       }//ends switch

       return objectNounString;

    }//ends objectNoun

    
     public static void finalSentence(){
       
       Random randomGenerator = new Random();
       int newSentenceInt = randomGenerator.nextInt(2);
         
       String sentenceString = "undefined";

       switch (newSentenceInt) {
       case 0: sentenceString = subjectNoun() + " was " + adjective() + " " + verb() + " to " + verb () + " " + objectNoun() + ". ";
          break;
        case 1: sentenceString = "It used " + subjectNoun() + " to " + verb() + " " + objectNoun() + " at the " + adjective() + " " + objectNoun() + ". ";
          break;
       }//ends switch
       
       String conclusion = "That " + subjectNoun() + " " + verb() + " her " + objectNoun() + "!";
       
       System.out.print(sentenceString); 
       
       System.out.println(conclusion);

       System.out.println(); 


     }
    
  }//ends class