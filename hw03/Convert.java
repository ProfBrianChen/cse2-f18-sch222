//////////////////////////
///Sarah Horne 
///September 18, 2018
///CSE 02 
///Homework 3 program #1 
///The purpose of this program is to convert the quantity
///of rain in a given region into cubic miles
//////////////////////////

import java.util.Scanner;

public class Convert{//Start Class
  
  public static void main (String args[]){//Start Main
    
    Scanner myScanner = new Scanner( System.in ); //Declare instance of the Scanner object
    
    System.out.println ("Enter the affected area in acres: "); //Asks user for affected area in acres
    double areaInAcres = myScanner.nextDouble(); //Accepts user input for affected area in acres
    
    System.out.println ("Enter the rainfall in the affected area: "); //Asks user for rainfall in inches
    double rainfallInInches = myScanner.nextDouble(); //Accepts user input for rainfall in inches

    double inchesToFeet = rainfallInInches/12; //Converts rainfall in inches to feet 
    double feetToMile = inchesToFeet/5280; //Converts rainfall in feet to miles
    
    double areaInSqMiles = areaInAcres/640; //Converts affected area in acres to square miles
    
    double cubicMiles = areaInSqMiles * feetToMile; //Finds cubic miles of rain by multiplying area in square miles and rainfall in miles
    
    System.out.println (cubicMiles + " cubic miles"); //Prints out the cubic miles 
 
    
  }//End Main
  
}//End Class



