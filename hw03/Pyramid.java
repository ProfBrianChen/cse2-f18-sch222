//////////////////////////
///Sarah Horne 
///September 18, 2018
///CSE 02 
///Homework 3 program #2 
///Gives the volume inside a pyramid by taking user inputs
//////////////////////////

import java.util.Scanner;

public class Pyramid{//Start Class
  
  public static void main (String args[]){//Start Main
    
    Scanner myScanner = new Scanner( System.in ); //Declare instance of the Scanner object
    
    System.out.println ("The square side of the pyramid is (input length): ");
    double squareSide = myScanner.nextDouble();
    
    System.out.println ("The height of the pyramid is (input height): ");
    double height = myScanner.nextDouble();
    
    double squareSideSquared = squareSide * squareSide;
    double heightDividedByThree = height / 3;
    
    double volume = squareSideSquared * heightDividedByThree;
    
    
    System.out.println ("The volume inside the pyramid is: " + volume);
    
  }//End Main
  
}//End Class




