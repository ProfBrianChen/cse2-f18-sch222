//////////////////////
/// Sarah Horne 
/// December 7, 2018
/// Lab 10- Selection sort
//////////////////////

import java.util.Arrays;

public class SelectionSortLab10{//Start Class
  public static void main (String [] args) {//Start main
    
    int [] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int [] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    
    int iterBest = selectionSort (myArrayBest);
    int iterWorst = selectionSort (myArrayWorst);
    
    System.out.println ("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println ("The total number of operations performed ont he reverse sorted array: " + iterWorst);
    
  }//End main
  
  /** The method for sorting the numbers **/
  public static int selectionSort (int [] list){
    // Prints the inital array (you must insert another
    // print out statement later in the code to show the
    // array as it's being sorted)
    
    System.out.println(Arrays.toString(list));
    
    int iterations = 0;
    
    for (int i = 0; i < list.length -1; i++) {
      //Update the iterations counter 
      iterations++;
      
      //Step 1: Find the minimum in the list 
      
      int currentMin = list [i];
      int currentMinIndex = i;
      
      for (int j = i + 1; j < list.length; j++){
        if (currentMin > list [j]){
          currentMin = list [j];
          currentMinIndex = j;
        }
        
        iterations++;
        
      }
      
      int temp; 
      //Step 2: Swap list [i] with the minimum you found above 
      if (currentMinIndex != i) {
        temp = list [i];
        list [i] = currentMinIndex; 
        currentMinIndex = temp;
        
      }
      
    }
    System.out.println(Arrays.toString(list));
    
    
    return iterations; 
    
    
  }
  
  
}// end class