//////////////////////
// Sarah Horne 
// December 7, 2018
// Lab 10- Insertion Sort
//////////////////////

import java.util.Arrays;

public class InsertionSortLab10 {//Start Class
  public static void main (String [] args) { //Start Main
   
    int [] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int [] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    
    int iterBest = insertionSort (myArrayBest);
    int iterWorst = insertionSort (myArrayWorst);
    
    System.out.println ("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println ("The total number of operations performed ont he reverse sorted array: " + iterWorst);
    
    
  } //End Main
  
  //The method for sorting the numbers
  public static int insertionSort (int [] list){//Start insertion
    // Prints the initial array (you must insert another 
    // print out statement later in the code)
    
    System.out.println(Arrays.toString(list));
    
    //Initialize counter for iterations 
    int iterations = 0;
    
    //for element list [i] in the array... 
    for (int i  = 1; i < list.length; i++){
      //Update the iterations counter 
      iterations ++;
      
      // Insert list[i] into a sorted sublist list[0..i-1] 
      // so that list[0..i] is sorted. 
      
      int temp;
      for (int j = i; j > 0; j --){
        if (list[j] < list[j-1]){
          temp = list [j];
          list [j] = list [j -1];
          list [j-1] = temp;
          iterations++;
        }
        else {
          break;
        }
      }
       System.out.println(Arrays.toString(list));
    }
    
    return iterations; 
    
  }//End insertion 
  
  
  
}//End class