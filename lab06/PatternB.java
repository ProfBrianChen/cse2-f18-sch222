//////////////////////////
/// Sarah Horne 
/// October 12, 2018
/// CSE 02
/// Lab 6- Pattern A
///
//////////////////////////

import java.util.Scanner;

public class PatternB{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 
   
    Scanner myScanner = new Scanner( System.in );

    int i = 0;
   
    while (i < 1){
      System.out.println ("Please enter an integer between 1 and 10");
      boolean countTest = myScanner.hasNextInt();
      if (countTest){
        int count = myScanner.nextInt();
        
        if (count >= 1 && count <= 10){
          for (int row = count; row >= 1; row--) {
            for (int number = 1; number <= row; number++) {
              System.out.print(number + " ");
            }
            System.out.println();
          }
          
          i++;
        }else {
          System.out.println ("Please enter a number from 1 to 10!");
        }
      } else 
      {
        System.out.println ("Error, please enter an int!");
        myScanner.next();
      }
    }

    
  }//End Main
  
}//End Class 

