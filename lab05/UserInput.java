//////////////////////////
/// Sarah Horne 
/// October 5, 2018
/// CSE 02
/// Lab 5- User Input
///
/// Purpose of lab:
//////////////////////////

import java.util.Scanner;

public class UserInput{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 
   
    Scanner myScanner = new Scanner( System.in );
    
    int i = 0; 
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    int n = 0; 
    
    
    while (i < 1){
      System.out.println ("What is the course number?");
      boolean courseNumberTest = myScanner.hasNextInt();
      if (courseNumberTest){
        int courseNumber = myScanner.nextInt();
        System.out.println ("The course number is: " + courseNumber);
        i++;
      } else 
      {
        System.out.println ("Error, please enter an int!");
        myScanner.next();
      }
    }
     
    
    while (j < 1){
      System.out.println ("What is the department name?");
      boolean departmentNameTest = myScanner.hasNext();
      if (departmentNameTest){
        String departmentName = myScanner.next();
        System.out.println ("The department name is: " + departmentName);
        j++;
      } else 
      {
        System.out.println ("Error, please enter a String!");
        myScanner.next();
      }
    }
    
    
    while (k < 1){
      System.out.println ("How many times a week does it meet?");
      boolean timesAWeekTest = myScanner.hasNextInt();
      if (timesAWeekTest){
        int timesAWeek = myScanner.nextInt();
        System.out.println ("The class meets " + timesAWeek + " times a week");
        k++;
      } else 
      {
        System.out.println ("Error, please enter an int!");
        myScanner.next();
      }
    }
    
    while (l < 1){
      System.out.println ("What time does class start?(Indicate as one number- for example if it is 4:30 make it 430)");
      boolean startTimeTest = myScanner.hasNextInt();
      if (startTimeTest){
        int startTime = myScanner.nextInt();
        System.out.println ("The class starts at: " + startTime);
        l++;
      } else 
      {
        System.out.println ("Error, please enter an int!");
        myScanner.next();
      }
    }
    
  while (m < 1){
      System.out.println ("What is the instructor's name?");
      boolean instructorNameTest = myScanner.hasNext();
      if (instructorNameTest){
        String instructorName = myScanner.next();
        System.out.println ("The instructor's name is: " + instructorName);
        m++;
      } else 
      {
        System.out.println ("Error, please enter a String!");
        myScanner.next();
      }
    }
    
  
    
    while (n < 1){
      System.out.println ("How many students are in the class?");
      boolean studentsTest = myScanner.hasNextInt();
      if (studentsTest){
        int students = myScanner.nextInt();
        System.out.println ("There are " + students + " students in the class.");
        n++;
      } else 
      {
        System.out.println ("Error, please enter an Int!");
        myScanner.next();
      }
    }
    
    
  }//End Main
  
}//End Class 





