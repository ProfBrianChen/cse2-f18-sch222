//////////////////////////
///Sarah Horne 
///September 11, 2018
///CSE 02 Arithmetic
///Homework 2
//////////////////////////

public class Arithmetic{//Start Class
  
  public static void main (String args[]){//Start Main
    
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants

    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //cost per belt

    double paSalesTax = 0.06; //the tax rate

    double totalCostPants; //Total cost of pants with no tax
    double totalCostShirts; //Total cost of shirts with no tax
    double totalCostBelts; //Total cost of belts with no tax
    
    double salesTaxPants; //Tax on pants
    double salesTaxShirts; //Tax on shirts
    double salesTaxBelts; //Tax on belts
    
    double totalCostOfPurchase; //Total cost of everything before tax 
    
    double totalCostPantsWithTax; //Total cost of pants with tax
    double totalCostShirtsWithTax; //Total cost of shirts with tax
    double totalCostBeltsWithTax; //Total cost of belts with tax
    double totalWithTax; //Total cost of purchase with Tax
    
    totalCostPants = numPants * pantsPrice;
    totalCostShirts = numShirts * shirtPrice;
    totalCostBelts = numBelts * beltPrice; 
    
    
    //Prints cost of pants pre tax which is $104.94
    System.out.println ("The total cost of the pants without tax is $" + totalCostPants + "."); 
    //Prints cost of shirts pre tax which is $49.98 
    System.out.println ("The total cost of the shirts without tax is $" + totalCostShirts + "."); 
     //Prints cost of the belt pre tax with is $33.99
    System.out.println ("The total cost of the belt without tax is $" + totalCostBelts + ".");
    
    salesTaxPants = totalCostPants * paSalesTax;
    salesTaxShirts= totalCostShirts * paSalesTax;
    salesTaxBelts = totalCostBelts * paSalesTax;
    
    totalCostPantsWithTax = salesTaxPants + totalCostPants;
    totalCostShirtsWithTax = salesTaxShirts + totalCostShirts;
    totalCostBeltsWithTax = salesTaxBelts + totalCostBelts;
    
    double pantsTaxTwoDigit = (double) Math.round(salesTaxPants * 100) / 100; //price of the tax on the pants, calculated with 2 decimal points
    double shirtsTaxTwoDigit = (double) Math.round(salesTaxShirts * 100) / 100; //price of the tax on the shirts, calculated with 2 decimal points
    double beltsTaxTwoDigit = (double) Math.round(salesTaxBelts* 100) / 100; //price of the tax on the belt, calculated with 2 decimal points
    
    double totalCostPantsWithTaxTwoDigits = (double) Math.round(totalCostPantsWithTax * 100) / 100; //Total price of pants, calculated with 2 decimal points
    double totalCostShirtsWithTaxTwoDigits = (double) Math.round(totalCostShirtsWithTax * 100) / 100; //Total price of shirts, calculated with 2 decimal points
    double totalCostBeltsWithTaxTwoDigits = (double) Math.round(totalCostBeltsWithTax * 100) / 100; //Total price of belts, calculated with 2 decimal points
    
    //Prints tax on the pants and then the price with tax
    System.out.println ("The total tax on the pants is $" + pantsTaxTwoDigit + ". And the total cost is now $" + totalCostPantsWithTaxTwoDigits + "."); 
    //Prints tax on the shirts and then the price with tax
    System.out.println ("The total tax on the shirts is $" + shirtsTaxTwoDigit + ". And the total cost is now $" + totalCostShirtsWithTaxTwoDigits + "." ); 
    //Prints tax on the belts and then the price with tax
    System.out.println ("The total tax on the belt is $" + beltsTaxTwoDigit + ". And the total cost is now $" + totalCostBeltsWithTaxTwoDigits + "." ); 
    
    
    
    totalCostOfPurchase= totalCostPants + totalCostShirts + totalCostBelts;
    totalWithTax= totalCostPantsWithTax + totalCostShirtsWithTax + totalCostBeltsWithTax; 
    
    double totalCostOfPurchaseTwoDigits = (double) Math.round(totalCostOfPurchase * 100) / 100; //Total cost of purchase without tax, calculated with 2 decimal points
    double totalWithTaxTwoDigits = (double) Math.round(totalWithTax * 100) / 100; //Total cost of purchase with tax, calculated with 2 decimal points
    
    
    //Prints total cost without tax
    System.out.println ("The total cost of the purchase without tax is $" + totalCostOfPurchaseTwoDigits + ".");
    //Prints total cost with tax
    System.out.println ("The total cost of the purchase with tax is $" + totalWithTaxTwoDigits + ".");
    
    
  }//End Main
  
}//End Class


