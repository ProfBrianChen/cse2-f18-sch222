//////////////////////////
/// Sarah Horne 
/// September 14, 2018
/// CSE 02
/// Lab 3- Check
///
/// Purpose of lab: Split bill between friends using the 
/// Scanner class 
//////////////////////////

import java.util.Scanner;

public class Check{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 
    
    Scanner myScanner = new Scanner( System.in ); //Declare instance of the Scanner object
    
    //Cost of check
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Asks user for the original cost of check
    double checkCost = myScanner.nextDouble(); //Accepts user input for cost of check with a double
    
    
    //Tip Percent
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Asks user for the tip percent they want 
    double tipPercent = myScanner.nextDouble(); //Accepts user input for tip in form of a double
    tipPercent /= 100; //Convert the percentage into a decimal value
      
    //Number of People
    System.out.print("Enter the number of people who went out to dinner: "); //Asks user for number of people at dinner
    int numPeople = myScanner.nextInt(); //Accepts user input for number of people 
    
    //Declare ints and doubles 
    double totalCost;
    double costPerPerson;
    int dollars;//whole dollar amount of cost 
    int dimes; 
    int pennies; // pennies and dimes are for storing digits to the right of the decimal point for the cost

    totalCost = checkCost * (1 + tipPercent); //Caclulates total cost with tip 
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson; //Gets dollar amount
    dimes=(int)(costPerPerson * 10) % 10; //Gets dime amount 
    pennies=(int)(costPerPerson * 100) % 10; //Gets pennie ammount
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//Outputs what each person owes 

    
  }//End Main
  
}//End Class 



