//////////////////////////
///Sarah Horne 
///September 2, 2018
///CSE 02 Welcome Message
///Homework 1
//////////////////////////

public class WelcomeClass{//Start Class
  
  public static void main (String args[]){//Start Main
    
    System.out.println("    -----------      ");
    System.out.println("    | WELCOME |      ");
    System.out.println("    -----------      ");
    System.out.println("   ^  ^  ^  ^  ^  ^  ");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\ "); 
    System.out.println(" <-S--C--H--2--2--2->"); 
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ / ");    
    System.out.println("   v  v  v  v  v  v  ");
    System.out.println("My name is Sarah Horne and I am a freshamn on the swim team! I am from Wayne, NJ.");
    
    /**Used Syste.out.println to print a few lines of 
    statement to the output. "println" is important because
    it enters a new line after the text in the quotation marks**/ 
    
    //In the statements on lines 16 and 18 I had to use double backslashes 
    //or "\\" because "\" causes an error as it is the escape key. 
    
    
  }//End Main
  
}//End Class

