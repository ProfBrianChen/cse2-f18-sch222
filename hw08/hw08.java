///////////////
/// CSE02 Homework 08
/// Sarah Horne 
/// November 15, 2018
///////////////
 
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


  public class hw08{//Start Class
    
    public static void main(String[] args){//Start main
      
      Scanner scan = new Scanner(System.in); 
      
      //suits club, heart, spade or diamond 
      String[] suitNames={"C","H","S","D"};    
      String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
      String[] cards = new String[52]; 
      String[] hand = new String[5]; 
      
      int again = 1; 
      int index = 51;
      
      System.out.print("Unshuffled list: ");
      //Prints out each element of the array with a space inbetween
      for (int i=0; i<52; i++){ 
        cards[i]=rankNames[i%13]+suitNames[i/13]; 
        System.out.print(cards[i]+" "); 
      } 
 
      //Print shuffled method using cards above
      System.out.println();
      shuffle(cards); 
      System.out.println();
      
      //Asks user how many cards they want 
      System.out.println("Enter the number of cards you want"); 
      int numCards = scan.nextInt(); 
      
      //While loop for if the person wants a nother hand of cards
      while(again == 1){ 
        System.out.println("Your hand: ");
        hand = getHand(cards,index,numCards); 
        printArray(hand);
        index = index - numCards;
        System.out.println();
        System.out.println("Enter a 1 if you want another hand drawn"); 
        again = scan.nextInt(); 
        
        //If statement to check if the user entered more than 52, making a new deck of cards
        if (index < numCards){
          index = 51;
          
          for (int i=0; i<52; i++){ 
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
            //System.out.print(cards[i]+" "); 
          } 
          
          shuffle(cards);
          System.out.println();
        
      }
      }

    }//End Main
    
    
    
    
    public static void printArray(String [] list){//Start printArray
        
      //Prints array with a space between each card
      for (int i=0; i < list.length; i++){ 
          System.out.print(list[i]+" "); 
        } 
      
      
    }//End printArray
    
    
    public static void shuffle(String[] list){//Start printArray
     
      //gets random number
      Random rand = new Random();
      int n = rand.nextInt(52);
      
      //Randomizes the order of the deck of cards
      for (int i = 0; i < 52; i++){ 
        int index = rand.nextInt(i + 1);
        String a = list[index];
        list[index] = list[i];
        list[i] = a;
      } 
    
      System.out.println("Shuffled List: " + Arrays.toString(list));  
      
      
    }//End shuffle
    
    
    
    public static String[] getHand(String[] list, int index, int numCards){//Start getHand
      
      
      int start = index;
      String[] a = new String [numCards];
      int j = 0;
      
      //Makes string for hand of cards of size numCards
      for (int i = start; j < numCards; i--){ 
        a[j] = list[i];
        start -= numCards;
        j++;
      }
      
     return a;
      
      
    }//End getHand
    
    
    
  }//ends class