///////////////
/// CSE02 hw 07
/// Sarah Horne 
/// October 30, 2018
///////////////

import java.util.Scanner;
import java.util.*;

public class WordTools {

 //Main method
  public static void main(String[] args){
    
    //Calls two methods- sampleText and printMenu
    
    System.out.println("You entered: " + sampleText());  
    System.out.println(printMenu()); 
    
    //Loop that loops the menu operation
    int i = 0; 
    while (i == 0){
      if (printMenu().equals("q")){
        System.out.println("Ending program");
        i = 1; 
      }else {
        System.out.println(printMenu());
        i = 0; 
      }
    }
  }
 
 //Sample text- accepts input from the user that will be evaluated later 
 public static String sampleText(){
    Scanner scanner = new Scanner(System.in);
    System.out.println ("Enter a sample text:");
    String sample = scanner.nextLine();
    
    return sample;
  } 
  
  
  //Prints a menu with options shown in the methods below 
   public static String printMenu(){
    Scanner scanner = new Scanner(System.in);
    System.out.println ("MENU\n"
                       + "c - Number of non-whitespace characters\n"
                       + "w - Number of words\n"
                       + "f - Find text\n"
                       + "r - Replace all !'s\n"
                       + "s - Shorten spaces\n"
                       + "q - Quit\n"
                       + "Choose an option: ");
     String option = scanner.nextLine();
     String c = "c";
     String w = "w";
     String f = "f";
     String r = "r";
     String s = "s";
     String q = "q";
     String editedIntro = "Edited text: ";
    
     //If else statment that goes through different scenarios of user input
     if (option.equals("c") || option.equals("w") || option.equals("f") || option.equals("r") || option.equals("s") || option.equals("q")){
         if (option.equals ("c")){
         return getNumOfNonWSCharacters(sampleText());
       } else if (option.equals ("w")){
         return getNumOfWords(sampleText());
       } else if (option.equals ("f")){
         return  findText(sampleText());
       } else if (option.equals ("r")){
         String replacedText= editedIntro + replaceExclamation(sampleText());
         return replacedText;
       } else if (option.equals ("s")){
         String editedShortenSpace= editedIntro + shortenSpace(sampleText());;
         return editedShortenSpace;
       }  else if (option.equals ("q")){
          
       }
     } else {
        System.out.println ("Error, please enter an option above!");
        return option;
     }
    
    return option;
  }
     
 
  //Gets the number of non space characters in the String inputted in sampleText
  public static String getNumOfNonWSCharacters(String sample){
    int i = 0;
    int c;     
    for(c = 0; i<sample.length(); i++)
        {
            char ch=sample.charAt(i);
            if(ch==' ')
            c++;
        }
      int length = sample.length( ); 
      int nonWhite = length - c;
      String nonWhiteString = "Number of non-whitespace characters: " + nonWhite;
      
        return nonWhiteString;
  }
  
  //Gets the number of words in the String inputted in sampleText
  public static String getNumOfWords(String sample){
    String[] words = sample.split("\\s+");
    int numWords = words.length;
    String numWordsString = "Number of words: " + numWords;
    return numWordsString;
    
  }
  
  
  //Finds a word, letter, or phrase in the String inputted in sampleText
  public static String findText(String sample){
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a word or phrase to be found: ");
    String find = scanner.nextLine();
    
    int index = sample.indexOf(find);
    int count = 0;
    while (index != -1) {
      count++;
      sample = sample.substring(index + 1);
      index = sample.indexOf(find);
     }
    
    String instances = "Instances found: " + count;
    return instances;
  }
  
  
  //Replaces the ! with . in the String inputted in sampleText
  public static String replaceExclamation(String sample){
    String s1= sample;  
    String replaceString=s1.replaceAll("!",".");//replaces all occurrences of "!" to "."  
    
    return replaceString;   
    
  }
  
   //puts only one space between the words in the String inputted in sampleText
   public static String shortenSpace(String sample){
    String s1= sample;  
    String shortenAndReplaceString=s1.replaceAll("  "," ");//replaces all occurrences of two spaces to one space 
    
    return shortenAndReplaceString;  
   
  }
  

}
