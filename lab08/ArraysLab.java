///////////////
/// CSE02 Arrays Lab08
/// Sarah Horne 
/// November 16, 2018
///////////////
 
import java.util.Random;
import java.util.Arrays;


  public class ArraysLab{//Start Class
    
    public static void main(String[] args){//Start main
      
      //2 arrays
      int [] arrayOne = new int[100];
      int [] arrayTwo = new int [100]; 
   
      //randomizes the 100 elements
      Random rand = new Random();
      for (int i = 0; i < arrayOne.length; i++) {
        int n = rand.nextInt(100);
        arrayOne[i] = n;
      }
        
      //Output that shows the array with numbers inside
      System.out.println("Array 1 holds the following integers: " + Arrays.toString(arrayOne));
      
      int j = 0; //temporary value
      int i = 0; 
      
      //Puts the numbers from arrayOne into arrayTwo 
      for (i = 0; i < arrayOne.length; i++){
                j = arrayOne[i];
                arrayTwo[j]++;
            }

      //Searches how many times it is found within the array 
      for (i = 1; i < arrayTwo.length; i++){
        //Checks how many times numbers 1-99 occur in the array and prints out the number    
        if(arrayTwo[i] == 1){
             System.out.println(i + " occurs " + arrayTwo [i] + " time ");
             }
            else if(arrayTwo[i] >=2 || arrayTwo[i] == 0){
                System.out.println(i + " occurs " + arrayTwo [i] + " times ");
            }


         }
      
   
      
    }//End Main
    
  }//ends class