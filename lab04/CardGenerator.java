//////////////////////////
/// Sarah Horne 
/// September 21, 2018
/// CSE 02
/// Lab 4- Card Generator
///
/// Purpose of lab: Use random number generator to select a number from 1 to 52
/// that represents 1 card in a deck  of cards. 
//////////////////////////

import java.lang.Math;

public class CardGenerator{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 
   
    int rank = (int)(Math.random()*12)+1; //Random Number for rank
    int suit = (int)(Math.random()*4)+1; //Random Number for suit
    
    
    //Goes through cases that asigns the random number to the rank of the card
    String cardString;
      switch (rank) {//Start switch
        case 1:  cardString = "Ace of ";
           break;
        case 2:  cardString = "2 of ";
           break;
        case 3:  cardString = "3 of ";
           break;
        case 4:  cardString = "4 of ";
           break;
        case 5:  cardString = "5 of ";
           break;
        case 6:  cardString = "6 of ";
           break;
        case 7:  cardString = "7 of ";
           break;
        case 8:  cardString = "8 of ";
           break;
        case 9:  cardString = "9 of ";
           break;
        case 10: cardString = "10 of ";
           break;
        case 11: cardString = "Jack of ";
           break;
        case 12: cardString = "Queen of ";
           break;
        case 13: cardString = "King of ";
           break;
        default: cardString = "Invalid Number";
           break;
     }//End Switch
    
      //Goes through cases that assigns a random numner to the suit of the card
      String cardFace;
        switch (suit){//Start Switch
          case 1: cardFace = "Diamonds";
            break;
          case 2: cardFace = "Clubs";
            break;
          case 3: cardFace = "Hearts";
            break;
          case 4: cardFace = "Spades";
            break;
          default: cardFace = "Invalid Number";
            break;
        }//End switch

    System.out.println ("You picked the " + cardString + cardFace);
    
  }//End Main
  
}//End Class 




