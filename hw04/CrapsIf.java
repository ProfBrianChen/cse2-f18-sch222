//////////////////////////
/// Sarah Horne 
/// September 25, 2018
/// CSE 02
/// Hw 4- Craps- CrapsIf
///
/// Code craps game using if statements
//////////////////////////

import java.lang.Math;
import java.util.Scanner;

public class CrapsIf{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 

    
    Scanner myScanner = new Scanner( System.in ); //Declare instance of the Scanner object
    
    int diceOne; //Integer used for random rolls
    int diceTwo; //Integer used for random rolls
    
    diceOne = (int)(Math.random()*6)+1; //Random number for dice 1
    diceTwo = (int)(Math.random()*6)+1; //Random number for dice 2
    
    //Prints out statement asking for user input on how they want the numbers to be picked
    System.out.println ("Would you like the computer to randomly cast the dice or would you like to pick 2 numbers? If you want random press 1. If you want to pick press 2.");
    double YesNoRandom = myScanner.nextDouble(); //Accepts user input. 1= random, 2= user pics, anything else = error
    
    //If- else statement 
    if (YesNoRandom == 1) { //if= random numbers used
      if ((diceOne == 1) && (diceTwo ==1)) {
      System.out.println ("You rolled 1 and 1: Snake Eyes");
      }else if  ((diceOne == 1) && (diceTwo ==2)){
        System.out.println ("You rolled 1 and 2: Ace Deuce");
      }else if  ((diceOne == 1) && (diceTwo ==3)){
        System.out.println ("You rolled 1 and 3: Easy Four");
      }else if  ((diceOne == 1) && (diceTwo ==4)){
        System.out.println ("You rolled 1 and 4: Fever Five");
      }else if  ((diceOne == 1) && (diceTwo ==5)){
        System.out.println ("You rolled 1 and 5: Easy Six");
      }else if  ((diceOne == 1) && (diceTwo ==6)){
        System.out.println ("You rolled 1 and 6: Seven Out");
      }else if  ((diceOne == 2) && (diceTwo ==1)){
        System.out.println ("You rolled 2 and 1: Ace Deuce");
      }else if  ((diceOne == 2) && (diceTwo ==2)){
        System.out.println ("You rolled 2 and 2: Hard Four");  
      }else if  ((diceOne == 2) && (diceTwo ==3)){
        System.out.println ("You rolled 2 and 3: Fever Five");  
      }else if  ((diceOne == 2) && (diceTwo ==4)){
        System.out.println ("You rolled 2 and 4: Easy Six"); 
      }else if  ((diceOne == 2) && (diceTwo ==5)){
        System.out.println ("You rolled 2 and 5: Seven Out"); 
      }else if  ((diceOne == 2) && (diceTwo ==6)){
        System.out.println ("You rolled 2 and 6: Easy Eight");  
      }else if  ((diceOne == 3) && (diceTwo ==1)){
        System.out.println ("You rolled 3 and 1: Easy Four");  
      }else if  ((diceOne == 3) && (diceTwo ==2)){
        System.out.println ("You rolled 3 and 2: Fever Five");
      }else if  ((diceOne == 3) && (diceTwo ==3)){
        System.out.println ("You rolled 3 and 3: Hard Six");  
      }else if  ((diceOne == 3) && (diceTwo ==4)){
        System.out.println ("You rolled 3 and 4: Seven Out");  
      }else if  ((diceOne == 3) && (diceTwo ==5)){
        System.out.println ("You rolled 3 and 5: Easy Eight"); 
      }else if  ((diceOne == 3) && (diceTwo ==6)){
        System.out.println ("You rolled 3 and 6: Nine"); 
      }else if  ((diceOne == 4) && (diceTwo ==1)){
        System.out.println ("You rolled 4 and 1: Fever Five"); 
      }else if  ((diceOne == 4) && (diceTwo ==2)){
        System.out.println ("You rolled 4 and 2: Easy Six"); 
      }else if  ((diceOne == 4) && (diceTwo ==3)){
        System.out.println ("You rolled 4 and 3: Seven Out"); 
      }else if  ((diceOne == 4) && (diceTwo ==4)){
        System.out.println ("You rolled 4 and 4: Hard Eight"); 
      }else if  ((diceOne == 4) && (diceTwo ==5)){
        System.out.println ("You rolled 4 and 5: Nine"); 
      }else if  ((diceOne == 4) && (diceTwo ==6)){
        System.out.println ("You rolled 4 and 6: Easy Ten"); 
      }else if  ((diceOne == 5) && (diceTwo ==1)){
        System.out.println ("You rolled 5 and 1: Easy Six"); 
      }else if  ((diceOne == 5) && (diceTwo ==2)){
        System.out.println ("You rolled 5 and 2: Seven Out");
      }else if  ((diceOne == 5) && (diceTwo ==3)){
        System.out.println ("You rolled 5 and 3: Easy Eight"); 
      }else if  ((diceOne == 5) && (diceTwo ==4)){
        System.out.println ("You rolled 5 and 4: Nine");
      }else if  ((diceOne == 5) && (diceTwo ==5)){
        System.out.println ("You rolled 5 and 5: Hard Ten"); 
      }else if  ((diceOne == 5) && (diceTwo ==6)){
        System.out.println ("You rolled 5 and 6: Yo-leven"); 
      }else if  ((diceOne == 6) && (diceTwo ==1)){
        System.out.println ("You rolled 6 and 1: Seven Out"); 
      }else if  ((diceOne == 6) && (diceTwo ==2)){
        System.out.println ("You rolled 6 and 2: Easy Eight");
      }else if  ((diceOne == 6) && (diceTwo ==3)){
        System.out.println ("You rolled 6 and 3: Nine"); 
      }else if  ((diceOne == 6) && (diceTwo ==4)){
        System.out.println ("You rolled 6 and 4: Easy Ten");
      }else if  ((diceOne == 6) && (diceTwo ==5)){
        System.out.println ("You rolled 6 and 5: Yo-leven");
      }else if  ((diceOne == 6) && (diceTwo ==6)){
        System.out.println ("You rolled 6 and 6: Boxcars"); 
      }
    } else if (YesNoRandom == 2) { //else if= numbers picked by user
      System.out.println ("Please input a number 1-6 for dice 1"); //prompts user for a number for dice 1
      int diceOneInput = myScanner.nextInt(); //stores input for dice 1
      System.out.println ("Please input a number 1-6 for dice 2"); //prompts user for a number for dice 2
      int diceTwoInput = myScanner.nextInt(); //stores input for dice 2
      
      if ((diceOneInput == 1) && (diceTwoInput ==1)) {
      System.out.println ("You rolled 1 and 1: Snake Eyes");
      }else if  ((diceOneInput == 1) && (diceTwoInput ==2)){
        System.out.println ("You rolled 1 and 2: Ace Deuce");
      }else if  ((diceOneInput == 1) && (diceTwoInput ==3)){
        System.out.println ("You rolled 1 and 3: Easy Four");
      }else if  ((diceOneInput == 1) && (diceTwoInput ==4)){
        System.out.println ("You rolled 1 and 4: Fever Five");
      }else if  ((diceOneInput == 1) && (diceTwoInput ==5)){
        System.out.println ("You rolled 1 and 5: Easy Six");
      }else if  ((diceOneInput == 1) && (diceTwoInput ==6)){
        System.out.println ("You rolled 1 and 6: Seven Out");
      }else if  ((diceOneInput == 2) && (diceTwoInput ==1)){
        System.out.println ("You rolled 2 and 1: Ace Deuce");
      }else if  ((diceOneInput == 2) && (diceTwoInput ==2)){
        System.out.println ("You rolled 2 and 2: Hard Four");  
      }else if  ((diceOneInput == 2) && (diceTwoInput ==3)){
        System.out.println ("You rolled 2 and 3: Fever Five");  
      }else if  ((diceOneInput == 2) && (diceTwoInput ==4)){
        System.out.println ("You rolled 2 and 4: Easy Six"); 
      }else if  ((diceOneInput == 2) && (diceTwoInput ==5)){
        System.out.println ("You rolled 2 and 5: Seven Out"); 
      }else if  ((diceOneInput == 2) && (diceTwoInput ==6)){
        System.out.println ("You rolled 2 and 6: Easy Eight");  
      }else if  ((diceOneInput == 3) && (diceTwoInput ==1)){
        System.out.println ("You rolled 3 and 1: Easy Four");  
      }else if  ((diceOneInput == 3) && (diceTwoInput ==2)){
        System.out.println ("You rolled 3 and 2: Fever Five");
      }else if  ((diceOneInput == 3) && (diceTwoInput ==3)){
        System.out.println ("You rolled 3 and 3: Hard Six");  
      }else if  ((diceOneInput == 3) && (diceTwoInput ==4)){
        System.out.println ("You rolled 3 and 4: Seven Out");  
      }else if  ((diceOneInput == 3) && (diceTwoInput ==5)){
        System.out.println ("You rolled 3 and 5: Easy Eight"); 
      }else if  ((diceOneInput == 3) && (diceTwoInput ==6)){
        System.out.println ("You rolled 3 and 6: Nine"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==1)){
        System.out.println ("You rolled 4 and 1: Fever Five"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==2)){
        System.out.println ("You rolled 4 and 2: Easy Six"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==3)){
        System.out.println ("You rolled 4 and 3: Seven Out"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==4)){
        System.out.println ("You rolled 4 and 4: Hard Eight"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==5)){
        System.out.println ("You rolled 4 and 5: Nine"); 
      }else if  ((diceOneInput == 4) && (diceTwoInput ==6)){
        System.out.println ("You rolled 4 and 6: Easy Ten"); 
      }else if  ((diceOneInput == 5) && (diceTwoInput ==1)){
        System.out.println ("You rolled 5 and 1: Easy Six"); 
      }else if  ((diceOneInput == 5) && (diceTwoInput ==2)){
        System.out.println ("You rolled 5 and 2: Seven Out");
      }else if  ((diceOneInput == 5) && (diceTwoInput ==3)){
        System.out.println ("You rolled 5 and 3: Easy Eight"); 
      }else if  ((diceOneInput == 5) && (diceTwoInput ==4)){
        System.out.println ("You rolled 5 and 4: Nine");
      }else if  ((diceOneInput == 5) && (diceTwoInput ==5)){
        System.out.println ("You rolled 5 and 5: Hard Ten"); 
      }else if  ((diceOneInput == 5) && (diceTwoInput ==6)){
        System.out.println ("You rolled 5 and 6: Yo-leven"); 
      }else if  ((diceOneInput == 6) && (diceTwoInput ==1)){
        System.out.println ("You rolled 6 and 1: Seven Out"); 
      }else if  ((diceOneInput == 6) && (diceTwoInput ==2)){
        System.out.println ("You rolled 6 and 2: Easy Eight");
      }else if  ((diceOneInput == 6) && (diceTwoInput ==3)){
        System.out.println ("You rolled 6 and 3: Nine"); 
      }else if  ((diceOneInput == 6) && (diceTwoInput ==4)){
        System.out.println ("You rolled 6 and 4: Easy Ten");
      }else if  ((diceOneInput == 6) && (diceTwoInput ==5)){
        System.out.println ("You rolled 6 and 5: Yo-leven"); 
      }else if  ((diceOneInput == 6) && (diceTwoInput ==6)){
        System.out.println ("You rolled 6 and 6: Boxcars"); 
      }else{
        System.out.println ("Error. Must enter a number 1-6 for both dice.");
      }
    } else{ //If number other than 1 or 2 is entered this error statement is shown
      System.out.println ("Error. Must enter either 1 or 2.");
    }
    
  }//End Main
  
}//End Class 






