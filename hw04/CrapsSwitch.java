//////////////////////////
/// Sarah Horne 
/// September 25, 2018
/// CSE 02
/// Hw 4- Craps- CrapsSwitch
///
/// Code craps game using switch statements
//////////////////////////

import java.lang.Math;
import java.util.Scanner;

public class CrapsSwitch{//Start Class
  
  //Main method required for every Java program
  public static void main (String args[]){//Start Main 

    Scanner myScanner = new Scanner( System.in ); //Declare instance of the Scanner object
    
    int diceOne; //Integer used for random rolls
    int diceTwo; //Integer used for random rolls
    
    diceOne = (int)(Math.random()*6)+1; //Random number for dice 1
    diceTwo = (int)(Math.random()*6)+1; //Random number for dice 2 
    
    System.out.println ("Would you like the computer to randomly cast the dice or would you like to pick 2 numbers? If you want random press 1. If you want to pick press 2.");
    double YesNoRandom = myScanner.nextDouble(); //Accepts user input. 1= random, 2= user pics, anything else = error
    
    if (YesNoRandom == 1) { //if= random numbers used
    switch (diceOne) {//Start switch
        case 1: 
            switch (diceTwo){
              case 1: System.out.println ("You rolled 1 and 1: Snake Eyes");
                break;
              case 2: System.out.println ("You rolled 1 and 2: Ace Deuce");
                break; 
              case 3: System.out.println ("You rolled 1 and 3: Easy Four");
                break; 
              case 4: System.out.println ("You rolled 1 and 4: Fever Five");
                break; 
              case 5: System.out.println ("You rolled 1 and 5: Easy Six");
                break; 
              case 6: System.out.println ("You rolled 1 and 6: Seven Out");
                break;
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
      case 2: 
         switch (diceTwo){
              case 1: System.out.println ("You rolled 2 and 1: Ace Deuce");
                break;
              case 2: System.out.println ("You rolled 2 and 2: Hard Four"); 
                break; 
              case 3: System.out.println ("You rolled 2 and 3: Fever Five");
                break; 
              case 4: System.out.println ("You rolled 2 and 4: Easy Six");
                break; 
              case 5: System.out.println ("You rolled 2 and 5: Seven Out");
                break; 
              case 6: System.out.println ("You rolled 2 and 6: Easy Eight");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
         case 3: 
         switch (diceTwo){
              case 1: System.out.println ("You rolled 3 and 1: Easy Four");
                break;
              case 2: System.out.println ("You rolled 3 and 2: Fever Five"); 
                break; 
              case 3: System.out.println ("You rolled 3 and 3: Hard Six");
                break; 
              case 4: System.out.println ("You rolled 3 and 4: Seven Out");
                break; 
              case 5: System.out.println ("You rolled 3 and 5: Easy Eight");
                break; 
              case 6: System.out.println ("You rolled 3 and 6: Nine");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        case 4: 
         switch (diceTwo){
              case 1: System.out.println ("You rolled 4 and 1: Fever Five"); 
                break;
              case 2: System.out.println ("You rolled 4 and 2: Easy Six");  
                break; 
              case 3: System.out.println ("You rolled 4 and 3: Seven Out"); 
                break; 
              case 4: System.out.println ("You rolled 4 and 4: Hard Eight");
                break; 
              case 5: System.out.println ("You rolled 4 and 5: Nine");
                break; 
              case 6: System.out.println ("You rolled 4 and 6: Easy Ten");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        case 5: 
         switch (diceTwo){
              case 1: System.out.println ("You rolled 5 and 1: Easy Six"); 
                break;
              case 2: System.out.println ("You rolled 5 and 2: Seven Out");  
                break; 
              case 3: System.out.println ("You rolled 5 and 3: Easy Eight"); 
                break; 
              case 4: System.out.println ("You rolled 5 and 4: Nine");
                break; 
              case 5: System.out.println ("You rolled 5 and 5: Hard Ten");
                break; 
              case 6: System.out.println ("You rolled 5 and 6: Yo-leven"); 
                break;
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
         case 6: 
         switch (diceTwo){
              case 1: System.out.println ("You rolled 6 and 1: Seven Out");  
                break;
              case 2: System.out.println ("You rolled 6 and 2: Easy Eight");  
                break; 
              case 3: System.out.println ("You rolled 6 and 3: Nine"); 
                break; 
              case 4: System.out.println ("You rolled 6 and 4: Easy Ten");
                break; 
              case 5: System.out.println ("You rolled 6 and 5: Yo-leven");
                break; 
              case 6: System.out.println ("You rolled 6 and 6: Boxcars"); 
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        default: System.out.println ("Invalid Number. Dice 1 and Dice 2 must be a number from 1-6.");
            break;
      }//End Switch
      
    } else if (YesNoRandom == 2) { //else if= numbers picked by user
      System.out.println ("Please input a number 1-6 for dice 1"); //prompts user for a number for dice 1
      int diceOneInput = myScanner.nextInt(); //stores input for dice 1
      System.out.println ("Please input a number 1-6 for dice 2"); //prompts user for a number for dice 2
      int diceTwoInput = myScanner.nextInt(); //stores input for dice 2
      
      switch (diceOneInput) {//Start switch
        case 1: 
            switch (diceTwoInput){
              case 1: System.out.println ("You rolled 1 and 1: Snake Eyes");
                break;
              case 2: System.out.println ("You rolled 1 and 2: Ace Deuce");
                break; 
              case 3: System.out.println ("You rolled 1 and 3: Easy Four");
                break; 
              case 4: System.out.println ("You rolled 1 and 4: Fever Five");
                break; 
              case 5: System.out.println ("You rolled 1 and 5: Easy Six");
                break; 
              case 6: System.out.println ("You rolled 1 and 6: Seven Out");
                break;
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
      case 2: 
         switch (diceTwoInput){
              case 1: System.out.println ("You rolled 2 and 1: Ace Deuce");
                break;
              case 2: System.out.println ("You rolled 2 and 2: Hard Four"); 
                break; 
              case 3: System.out.println ("You rolled 2 and 3: Fever Five");
                break; 
              case 4: System.out.println ("You rolled 2 and 4: Easy Six");
                break; 
              case 5: System.out.println ("You rolled 2 and 5: Seven Out");
                break; 
              case 6: System.out.println ("You rolled 2 and 6: Easy Eight");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
         case 3: 
         switch (diceTwoInput){
              case 1: System.out.println ("You rolled 3 and 1: Easy Four");
                break;
              case 2: System.out.println ("You rolled 3 and 2: Fever Five"); 
                break; 
              case 3: System.out.println ("You rolled 3 and 3: Hard Six");
                break; 
              case 4: System.out.println ("You rolled 3 and 4: Seven Out");
                break; 
              case 5: System.out.println ("You rolled 3 and 5: Easy Eight");
                break; 
              case 6: System.out.println ("You rolled 3 and 6: Nine");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        case 4: 
         switch (diceTwoInput){
              case 1: System.out.println ("You rolled 4 and 1: Fever Five"); 
                break;
              case 2: System.out.println ("You rolled 4 and 2: Easy Six");  
                break; 
              case 3: System.out.println ("You rolled 4 and 3: Seven Out"); 
                break; 
              case 4: System.out.println ("You rolled 4 and 4: Hard Eight");
                break; 
              case 5: System.out.println ("You rolled 4 and 5: Nine");
                break; 
              case 6: System.out.println ("You rolled 4 and 6: Easy Ten");
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        case 5: 
         switch (diceTwoInput){
              case 1: System.out.println ("You rolled 5 and 1: Easy Six"); 
                break;
              case 2: System.out.println ("You rolled 5 and 2: Seven Out");  
                break; 
              case 3: System.out.println ("You rolled 5 and 3: Easy Eight"); 
                break; 
              case 4: System.out.println ("You rolled 5 and 4: Nine");
                break; 
              case 5: System.out.println ("You rolled 5 and 5: Hard Ten");
                break; 
              case 6: System.out.println ("You rolled 5 and 6: Yo-leven"); 
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
         case 6: 
         switch (diceTwoInput){
              case 1: System.out.println ("You rolled 6 and 1: Seven Out");  
                break;
              case 2: System.out.println ("You rolled 6 and 2: Easy Eight");  
                break; 
              case 3: System.out.println ("You rolled 6 and 3: Nine"); 
                break; 
              case 4: System.out.println ("You rolled 6 and 4: Easy Ten");
                break; 
              case 5: System.out.println ("You rolled 6 and 5: Yo-leven");
                break; 
              case 6: System.out.println ("You rolled 6 and 6: Boxcars"); 
                break; 
              default: System.out.println ("Invalid. Dice 2 must be a number from 1-6.");
                break;
            }
           break;
        default: System.out.println ("Invalid. Dice 1 and Dice 2 must be a number from 1-6.");
            break;
      }//End Switch
      
    } else { //If number other than 1 or 2 is entered this error statement is shown
      System.out.println ("Error. Must enter either 1 or 2.");
    }
     
      
  }//End Main
  
}//End Class 





