///////////////
/// CSE02 Arrays Hw10
/// Sarah Horne 
/// December 4, 2018
///////////////
 
import java.util.Arrays;
import java.util.Scanner;


  public class HW10{//Start Class
    
    public static void main(String[] args){//Start main
     
      
      Scanner myScanner = new Scanner( System.in ); 
      
      char[][] ticTacArray = { //Original tic tac toe board, will be minupulated during game
        {'1', '2', '3'},
        {'4', '5', '6'},
        {'7', '8', '9'},
      };
      
      //Prints out the multidimensional aray
      for (int row = 0; row < ticTacArray.length; row++){
        for (int column = 0; column < ticTacArray[row].length; column++){
          System.out.print (ticTacArray[row][column] + " ");
        }
        System.out.println();
      }
      
      //counter for while statement
      int j = 0;
      
     while (j < 10){ //Only 9 spaces on the board. 10 numbers used because 9 is only reached when the game results in a tie
       if (j < 9){ //0-8 which will be used for the board
         System.out.println("Please enter either 'x' or 'o'."); //Enter player
        char player = myScanner.next().charAt(0); //save which player
        
        if (player == 'x' || player == 'X'){ //check to see if they are player x, if they are continue 
          
          System.out.println("Enter the number of the spot you want to replace."); //pick which spot 1-9
          int position = myScanner.nextInt(); //save which spot
          
          if (position > 0 && position <= 9){ // If the position is allowed (1-9)
            
            int value = check(ticTacArray, player, position); //Check to see if value has already been used
              
            if (value == 0){ //when value = 0 there is no x or o there
                TicTac(ticTacArray, player, position); //Prints the new game board with the spots taken
                boolean win = winner(ticTacArray); //checks to see if a winner has been found
                
                if (win == true){ //When a player wins
                  System.out.println("You won!"); //Print win statment
                  j = 10; //j equals 10 which ends the while loop
                }else { // when there is no winner after a turn
                  j++; //add another number to j to start the while loop again and ask the next player for input
                }
                
              }else if (value == 1){ //When value = 1 then that space is taken
                System.out.println("Error, please enter a number not used."); //Print statement to get a new number
              }//end if else
            
          }// end position if 
          else{ //When the number is outside of domain (1-9)
            System.out.println ("Error, please enter an int from 1 to 9!"); //Ask user for a new number
          }
          
        }else if (player == 'o' || player == 'O'){//check to see if they are player o, if they are continue 
          System.out.println("Enter the number of the spot you want to replace.");//pick which spot 1-9
          int position = myScanner.nextInt();//save which spot
          
          
          if (position > 0 && position <= 9){ // If the position is allowed (1-9)
            int value = check(ticTacArray, player, position); //Check to see if value has already been used
              
              if (value == 0){ //when value = 0 there is no x or o there
                TicTac(ticTacArray, player, position);  //Prints the new game board with the spots taken
                boolean win = winner(ticTacArray); //checks to see if a winner has been found
                
                if (win == true){ //When a player wins
                  System.out.println("You won!"); //Print win statment
                  j = 10; //j equals 10 which ends the while loop
                }else {// When there is no winner after a turn
                  j++; //add another number to j to start the while loop again and ask the next player for input
                }
                
              }else if (value == 1){ //When value = 1 then that space is taken
                System.out.println("Error, please enter a number not used."); //Print statement to get a new number
              } //end if else
            
          }// end position if 
          else{  //When the number is outside of domain (1-9)
            System.out.println ("Error, please enter an int from 1 to 9!"); //Ask user for a new number
          }
        
        } else if (player != 'x' && player != 'o'){ //If neither x or o are entered
          System.out.println("Invalid input. Please select 'x' or 'o'"); //Reselect letter 
        }
         
       }else if (j == 9){ //When the result is a draw
         System.out.println("Draw!"); //Print draw statement 
         j++; //break out of while loop
       }
        
      }//End while
      
   }//End Main
    
    public static void TicTac (char [] [] beforeArray, char player, int position){ //Finds the inputted position and switches it with x or o
      
      
      //Switch statement that says which number is being replaced. Case 1 = 1 on the array and so on 
      switch (position){
          case 1: beforeArray [0][0] = player;
            break;
          case 2: beforeArray [0][1] = player;
            break; 
          case 3: beforeArray [0][2] = player;
            break; 
          case 4: beforeArray [1][0] = player;
            break; 
          case 5: beforeArray [1][1] = player;
            break; 
          case 6: beforeArray [1][2] = player;
            break;
          case 7: beforeArray [2][0] = player;
            break;
          case 8: beforeArray [2][1] = player;
            break;
          case 9: beforeArray [2][2] = player;
            break;
          default: System.out.println ("Invalid. Must be a number from 1-9.");
            break;
            }
      
      //Prints out the new array 
      for (int row = 0; row < beforeArray.length; row++){
        for (int column = 0; column < beforeArray[row].length; column++){
            System.out.print (beforeArray[row][column] + " ");
        }
        System.out.println();
      }
    }//End position method 
    
    
    public static int check (char [] [] beforeArray, char player, int position){ //Checks whether there is an x or o already in the spot wanted 
     
      //Initialize checkValue  
      int checkValue = 0; 
      
      //Switch that takes the position requested and sees if there is an x or o already there
      //If the spot is taken then 1 is returned and the while loop in the main makes you pick a new number 
      //If the spot is not taken then 0 is returned and there is no problem with the placement 
      switch (position){
          case 1: if (beforeArray [0][0] == 'x' || beforeArray [0][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0; 
                }
            break;
          case 2: if (beforeArray [0][1] == 'x' || beforeArray [0][1] == 'o'){
                checkValue = 1;}else{
                checkValue = 0; 
                }
                break; 
          case 3: if (beforeArray [0][2] == 'x' || beforeArray [0][2] == 'o'){
                checkValue = 1;} else{
                checkValue = 0; 
                }
            break; 
          case 4: if (beforeArray [1][0] == 'x' || beforeArray [1][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0; 
                }
            break; 
          case 5: if (beforeArray [1][1] == 'x' || beforeArray [1][1] == 'o'){
                checkValue = 1;} else{
                checkValue = 0; 
                }
            break; 
          case 6: if (beforeArray [1][2] == 'x' || beforeArray [1][2] == 'o'){
                checkValue = 1;} else{
                checkValue = 0; 
                }
            break;
          case 7: if (beforeArray [2][0] == 'x' || beforeArray [2][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 8: if (beforeArray [2][1] == 'x' || beforeArray [2][1] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 9: if (beforeArray [2][2] == 'x' || beforeArray [2][2] == 'o'){
                checkValue = 1;} else {
                checkValue = 0;}
            break;
          default: System.out.println ("Invalid.");
            break;
            }
      
       return checkValue; 
    }//End check method 
    
    
    
    public static boolean winner (char [][] finalArray){ //Checks to see if a winner is found in the current array 
      
      //If statements go through every possibility 
      if (finalArray [0][0] == 'x' && finalArray [0][1] == 'x' && finalArray [0][2] == 'x'){
        return true;
      } else if (finalArray [1][0] == 'x' && finalArray [1][1] == 'x' && finalArray [1][2] == 'x'){
        return true;
      } else if (finalArray [2][0] == 'x' && finalArray [2][1] == 'x' && finalArray [2][2] == 'x'){
        return true;
      } else if (finalArray [0][0] == 'x' && finalArray [0][1] == 'x' && finalArray [0][2] == 'x'){
        return true;
      }else if (finalArray [1][0] == 'x' && finalArray [1][1] == 'x' && finalArray [1][2] == 'x'){
        return true;
      }else if (finalArray [2][0] == 'x' && finalArray [2][1] == 'x' && finalArray [2][2] == 'x'){
        return true;
      }else if (finalArray [0][0] == 'x' && finalArray [1][1] == 'x' && finalArray [2][2] == 'x'){
        return true;
      }else if (finalArray [0][2] == 'x' && finalArray [1][1] == 'x' && finalArray [2][0] == 'x'){
        return true;
      }else if (finalArray [0][0] == 'o' && finalArray [0][1] == 'o' && finalArray [0][2] == 'o'){
        return true;
      } else if (finalArray [1][0] == 'o' && finalArray [1][1] == 'o' && finalArray [1][2] == 'o'){
        return true;
      } else if (finalArray [2][0] == 'o' && finalArray [2][1] == 'o' && finalArray [2][2] == 'o'){
        return true;
      } else if (finalArray [0][0] == 'o' && finalArray [0][1] == 'o' && finalArray [0][2] == 'o'){
        return true;
      }else if (finalArray [1][0] == 'o' && finalArray [1][1] == 'o' && finalArray [1][2] == 'o'){
        return true;
      }else if (finalArray [2][0] == 'o' && finalArray [2][1] == 'x' && finalArray [2][2] == 'o'){
        return true;
      }else if (finalArray [0][0] == 'o' && finalArray [1][1] == 'o' && finalArray [2][2] == 'o'){
        return true;
      }else if (finalArray [0][2] == 'o' && finalArray [1][1] == 'o' && finalArray [2][0] == 'o'){
        return true;
      }else {
        return false; 
      }
      
    }//end winner method 
    
    
  }//ends class